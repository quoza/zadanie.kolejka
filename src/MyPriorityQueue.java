import java.util.ArrayList;
import java.util.List;

public class MyPriorityQueue {
    private List<QueueElement> myPriorityQueue = new ArrayList<>();
    private Boolean inc;

    public MyPriorityQueue(boolean incr) {
    }

    public void push(QueueElement element){
        if(inc) {
            for (int i = 0; i < myPriorityQueue.size(); i++) {
                if (myPriorityQueue.get(i).getPriority() < element.getPriority()) {
                    myPriorityQueue.add(i, element);
                    break;
                }
            }
        } else {
            for (int i = 0; i < myPriorityQueue.size(); i++) {
                if (myPriorityQueue.get(i).getPriority() > element.getPriority()) {
                    myPriorityQueue.add(i, element);
                    break;
                }
            }
        }
    }

    public QueueElement pop(int index){
        QueueElement x = myPriorityQueue.get(index);
        myPriorityQueue.remove(index);
        return x;
    }

    public int size(){
        return myPriorityQueue.size();
    }

    public void print(){
        for (QueueElement element : myPriorityQueue){
            System.out.println(element);
        }
    }
}