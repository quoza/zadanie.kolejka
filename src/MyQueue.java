import java.util.ArrayList;
import java.util.List;

public class MyQueue<T> {
    private List<T> myQueue = new ArrayList<>();

    public MyQueue() {
    }

    public void push_back(T object) {
        myQueue.add(object);
    }

    public T pop_front(T object) {
        if (myQueue.isEmpty()) {
            throw new IndexOutOfBoundsException();
        } else {
            T x = myQueue.get(0);
            myQueue.remove(0);
            return x;
        }
    }

    public T peek(T object) {
        if (myQueue.isEmpty()) {
            throw new IndexOutOfBoundsException();
        } else {
            return myQueue.get(0);
        }
    }

    public int size(T object) {
        return myQueue.size();
    }

    public void push_front(T object) {
        myQueue.add(0, object);
    }

    public T pop_back(T object) {
        if (myQueue.isEmpty()) {
            throw new IndexOutOfBoundsException();
        } else {
            T x = myQueue.get(myQueue.size());
            myQueue.remove(myQueue.size());
            return x;
        }
    }
}