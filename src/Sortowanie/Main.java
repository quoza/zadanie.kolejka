package Sortowanie;

public class Main {
    public static void main(String[] args) {
        Sort sort = new Sort();
        int[] array = new int[]{150, 3, 5, 12, 1, 2, 8, 6, 120, 23, 16, 6, 5, 8, 19, 22};

        sort.quickSort(array);
        for (int i : array) {
            System.out.print(i + " ");
        }
    }
}
